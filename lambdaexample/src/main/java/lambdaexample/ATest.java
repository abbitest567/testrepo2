package lambdaexample;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class ATest implements RequestHandler<String, String> {

    
	public String handleRequest(String input, Context context) {
    	String output = "Hello, " + input + "!";
    	System.out.println("hello git test");
    	return output;
    }
}